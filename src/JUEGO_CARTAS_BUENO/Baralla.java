package JUEGO_CARTAS_BUENO;

/**
 * Importo de las utilidades de Java, cosas que me hacen falta, ArrayList para hace una lista, Collections 
 * para hacer colecciones
 */
import java.util.ArrayList;
import java.util.Collections;

public class Baralla {
	/**
	 * Estoy Creando una Lista del tipo ArrayList que guarde Carta y la ArrayList la
	 * he llamado cartes
	 * 
	 */
	private ArrayList<Carta> cartes;

	/**
	 * Función para crear una baraja de cartas
	 */
	public Baralla() {

		cartes = new ArrayList<>();
		for (Pal pal : Pal.values()) {
			for (Valor valor : Valor.values()) {
				cartes.add(new Carta(pal, valor));
			}
		}

	}

	/**
	 * Función para desordenar las cartas
	 */
	public void barrejar() {
		Collections.shuffle(cartes);
	}

	/**
	 * Función para sacar una carta
	 * 
	 * @return
	 */
	public Carta treureCarta() {
		if (cartes.isEmpty()) {
			return null;
		}
		return cartes.remove(cartes.size() - 1);
	}
}
